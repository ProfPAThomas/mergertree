! Routines to convert strings between Fortran and C types

!--------------------------------

pure function mt_str_f2c(string) result(c_string)
!character*(csize) function mt_str(string)

! Appends null character to Fortran strings, for passing to C

  ! Need to load up the C string termination character.
  ! All string constants should be terminated with C_NULL_CHAR.
  use iso_c_binding

  integer, parameter :: csize=1024
  character*(*), intent(in) :: string
  character(c_char) :: c_string*(csize)

  c_string=string(1:len_trim(string))//C_NULL_CHAR
  
end function mt_str_f2c

!--------------------------------

pure function mt_str_c2f(c_string) result(string)

! Blanks out null character and all to the right, for passing
! String from C to Fortran
! I don't think that this works.  May do if recast as a subroutine.

  ! Need to load up the C string termination character.
  ! All string constants should be terminated with C_NULL_CHAR.
  use iso_c_binding

  integer, parameter :: csize=1024
  ! Does not work if I declare c_string properly as c_char
  !character(c_char), intent(in) :: c_string(*)
  character(*), intent(in) :: c_string
  character*(csize) :: string
  
  string=c_string(1:index(c_string,C_NULL_CHAR)-1)
  
end function mt_str_c2f

