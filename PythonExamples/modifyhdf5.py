# Example of how to modify the data in an existing HDF5 file

#-------------------------------------------------------------------------------
# Imports

import sys
import numpy
import h5py

#-------------------------------------------------------------------------------

# Open file
fid = h5py.File('test.hdf5', 'r+')

# Open merger tree for reading
gid = fid['/MergerTree']

# Create pointers to the datasets that we want to change
density=gid.get('Density')
mass=gid.get('Mass')

# Amend the data
d1=200.
d2=500.
d3=1000.
density[:]=[d1,d2,d3,d2,d2,d2,d3,d1,d1,d2,d2,d1,d1,d1,d1]
m1=1e15
m2=3e14
m3=1e14
m4=3e13
m5=1e13
mass[:]=[m1,m2,m3,m3,m4,m4,m5,m2,m3,m4,m4,m2,m3,m3,m3]

# Close up properly
fid.close()

