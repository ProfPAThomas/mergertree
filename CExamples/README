Files to give eamples of how to read and write merger trees in the standard
nIFTy format in HDF5.

----------------

Driver routines:

dumptree.c
  An example of how to write a merger tree in HDF5 format.
  First loads in some data by reading an existing merger tree, then
  (partially) recreates it.

readtree.c
  An example of how to read in an existing merger tree in HDF5 format.

----------------

Header files:

mt.h
  Function prototypes

mt_data.h
  Defines the structures needed to hold the merger tree and other data

----------------

Utility routines:

mt_close.c
  Close the HDF5 file

mt_dump_mergertree.c
  Write the MergerTree to the file

mt_dump_snaptable.c
  Write the Snap table to the file

mt_open.c
  Open an HDF5 file for reading and/or writing

mt_read_mergertree.c
  Read the MergerTree from the file

mt_read_snaptable.c
  Read the Snap table from the file

mt_tidy.c
  Free open resources

----------------

Directories:

data@ - link to directory that holds the test data

objects/ - directory to hold object files

tmp/ - temporary directory used during compilation to hold pre-processed files

----------------

Helper files:

Makefile - for use with make

README - this file
