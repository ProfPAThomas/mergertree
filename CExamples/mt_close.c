// Closes a file and stops HDF5

#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

// HDF5
#include "hdf5.h"
// Function prototypes
#include "mt.h"
// Global data
#include "mt_data.h"

int mt_close(void) {

  int ier;
  
  // Close file
  ier=mt_fclose(mt_fid);

  // Shutdown HDF5
  ier=mt_h5close();

  return ier;

}
