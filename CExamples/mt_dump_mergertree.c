//Dumps Merger Tree data to an HDF file in the official mt format

#define MTTABLE_INIT

#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "hdf5.h"
#include "hdf5_hl.h"
#include "mt.h"
#include "mt_data.h"

int mt_dump_mergertree(void) {

  // ToDo: add in possibility to write as a table 
  
  hid_t gid;
  int NHaloint; // To avoid compilation warning all this does is change type to int

  /* First check to see whether the MergerTree group exists and either open it 
   * or make it */
  gid=mt_gopen(mt_fid,"MergerTree");

  // Write the attributes
  NHaloint=(int)NHalo;
  mt_aset_int(gid, ".", "NHalo", NHaloint);
  mt_aset_int(gid, ".", "HaloIndexOffset", HaloIndexOffset);
  mt_aset_int(gid, ".", "TableFlag", TableFlag);

  // Write the arrays and associated attributes
  // HaloID
  mt_dmake(gid, "HaloID", NHalo, "int64", mtHaloID);
  mt_aset_string(gid, "HaloID", "Description", "ID in original catalogue");
  mt_aset_string(gid, "HaloID", "Units", "None");
  // Snapshot
  mt_dmake(gid, "Snapshot", NHalo, "int32", mtSnapshot);
  mt_aset_string(gid, "Snapshot", "Description", "Snapshot this halo resides in");
  mt_aset_string(gid, "Snapshot", "Units", "None");
  // Density
  mt_dmake(gid, "Density", NHalo, "float", mtDensity);
  mt_aset_string(gid, "Density", "Description", 
		 "Overdensity of this halo relative to critical density at that redshift");
  mt_aset_string(gid, "Density", "Units", "None");
  // Mass
  mt_dmake(gid, "Mass", NHalo, "float", mtMass);
  mt_aset_string(gid, "Mass", "Description", "Mass of halo");
  mt_aset_string(gid, "Mass", "Units", "Msun");
  // FirstSubhalo
  mt_dmake(gid, "FirstSubhaloIndex", NHalo, "int32", mtFirstSubhaloIndex);
  mt_aset_string(gid, "FirstSubhaloIndex", "Description", "Pointer to FirstSubhalo");
  mt_aset_string(gid, "FirstSubhaloIndex", "Units", "None");
  // Neighbour
  mt_dmake(gid, "NeighbourIndex", NHalo, "int32", mtNeighbourIndex);
  mt_aset_string(gid, "FirstSubhaloIndex", "Description", "Pointer to FirstSubhalo");
  mt_aset_string(gid, "FirstSubhaloIndex", "Units", "None");
  // HostHalo
  mt_dmake(gid, "HostHaloIndex", NHalo, "int32", mtHostHaloIndex);
  mt_aset_string(gid, "HostHaloIndex", "Description", "Pointer to HostHalo");
  mt_aset_string(gid, "HostHaloIndex", "Units", "None");
  // FirstProgenitor
  mt_dmake(gid, "FirstProgenitorIndex", NHalo, "int32", mtFirstProgenitorIndex);
  mt_aset_string(gid, "FirstProgenitorIndex", "Description", "Pointer to FirstProgenitor");
  mt_aset_string(gid, "FirstProgenitorIndex", "Units", "None");
  // NextSibling
  mt_dmake(gid, "NextSiblingIndex", NHalo, "int32", mtNextSiblingIndex);
  mt_aset_string(gid, "NextSiblingIndex", "Description", "Pointer to NextSibling");
  mt_aset_string(gid, "NextSiblingIndex", "Units", "None");
  // Descendant
  mt_dmake(gid, "DescendantIndex", NHalo, "int32", mtDescendantIndex);
  mt_aset_string(gid, "DescendantIndex", "Description", "Pointer to Descendant");
  mt_aset_string(gid, "DescendantIndex", "Units", "None");
  
  // Now close the open group
  mt_gclose(gid);

  return 0;

}
