// Reads in test HDF5 file so that dumptree.c can dump it out again

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// HDF5
#include "hdf5.h"

// Load function prototypes
#include "mt.h"

// Define the structure that we want to read in.
#include "mt_data.h"

int read_mock_data(char *infile){

  char description[CSIZE];

  // Open data file
  mt_open(infile,"r",description);

  // Read top-level attributes
  mt_aget_float(mt_fid,".","BoxsizeMpc",&box);
  mt_aget_float(mt_fid,".","H100",&h100);
  mt_aget_float(mt_fid,".","OmegaBaryon",&omega_baryon);
  mt_aget_float(mt_fid,".","OmegaCDM",&omega_cdm);
  mt_aget_float(mt_fid,".","OmegaLambda",&omega_lambda);
  mt_aget_float(mt_fid,".","Sigma8",&sigma8);
  mt_aget_string(mt_fid,".","Title",title);

  // Read SnapTable from HDF file
  mt_read_snaptable();

  // Read MergerTree from HDF file
#ifdef MTTABLE
  printf("***Routines not yet written to read in from MergerTree tables***\n");
#else
  mt_read_mergertree();
#endif

  // Close file
  mt_close();

  return 0;
}

