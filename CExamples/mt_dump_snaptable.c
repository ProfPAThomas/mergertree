//Dumps a Snapshot Table to an HDF file in the official mt format

// The following may be needed if mt_read_snaptable has not been called
// #define SNAPTABLE_INIT

#include <stdlib.h>
#include <string.h>
#include "hdf5.h"
#include "hdf5_hl.h"
#include "mt.h"
#include "mt_data.h"

int mt_dump_snaptable(void) {

  hid_t gid;
  hsize_t chunk_size;

  /* First check to see whether the Snapshots group exists and either open it 
   * or make it */
  gid=mt_gopen(mt_fid,"Snapshots");

  // Write the NSnap attribute
  mt_aset_int(gid, ".", "NSnap", (int)NSnap);

  /* Now we are going to dump the Snap table.
   * For this short table, set the chunk-size (which I think is in units of records)
   * equal to the number of records */
  chunk_size=NSnap;
  // Create and write the table.  I have left out parameters that seem redundant
  mt_tmake(gid,"Snap",snaptable_nfields,snaptable_field_names,
	   snaptable_field_types,NSnap,chunk_size,Snap);

  // Dump the SnapProp table
  mt_tmake(gid,"SnapProp",snapprop_nfields,snapprop_field_names,
	       snapprop_field_types,snaptable_nfields,snaptable_nfields,SnapProp);

  // Now close the open group
  mt_gclose(gid);

  return 0;

}
