subroutine mt_close()

! Closes merger tree file and shuts down HDF5

  use iso_c_binding
  use hdf5
  use mt
  use mt_data

  integer(c_int) :: ier

  ! Close file
  call mt_fclose(mt_fid)

  ! Close down Fortran interface to HDF5
  ! This and H5open_f are the only calls in the mt libraries that are not routed 
  ! through the corresponding function in the C version of the HDF5 library.
  call H5close_f(ier)

  ! Close down HDF5
  call mt_h5close()

end subroutine mt_close
