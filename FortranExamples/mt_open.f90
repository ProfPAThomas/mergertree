subroutine mt_open(file,ioflags,comment)

! Starts HDF5 and opens a file
! These routines to translate to/from C-routines look quite clever,
! but I don't think they actually work.

  use hdf5
  use iso_c_binding
  use mt
  use mt_data

  implicit none

  character*(*), intent(in) :: file     ! Name of file to open
  character*(*), intent(in) :: ioflags  ! read/write string;
  character*(*), intent(inout) :: comment    ! Comment to read/write to file

  ! C versions of input strings
  character(c_char) :: c_file*(csize)
  character(c_char) :: c_ioflags*(csize)
  character(c_char) :: c_comment*(csize)

  integer :: ier

  ! Start HDF5
  call mt_h5open()

  ! To get access to some Fortran data-types, need to start fortran HDF5 interface too.
  ! This and h5close_f are the only calls in the mt libraries that are not routed 
  ! through the corresponding function in the C version of the HDF5 library.
  call H5open_f(ier)

  ! Translate input strings to C-type
  c_file=mt_str_f2c(file)
  c_ioflags=mt_str_f2c(ioflags)
  c_comment=mt_str_f2c(comment)

  ! Call equivalent C routine
  mt_fid=mt_fopen(c_file,c_ioflags,c_comment)

  ! Translate comment string back to Fortran
  comment=mt_str_c2f(c_comment)

end subroutine mt_open
