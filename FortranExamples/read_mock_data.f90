subroutine read_mock_data(infile)

  ! We need to load up the HDF5 type definitions
  use hdf5

  ! Load function interfaces
  use mt

  ! This defines the merger tree structures as per the standard
  ! and defines some global vairables.
  use mt_data

  implicit none

  character*(CSIZE), intent(in) :: infile
  character*(CSIZE) :: description

  call mt_open(infile,'r',description)

  ! Read top-level attributes
  call mt_aget_float(mt_fid,mt_str_f2c('.'),mt_str_f2c('BoxsizeMpc'),box);
  call mt_aget_float(mt_fid,mt_str_f2c('.'),mt_str_f2c('H100'),h100);
  call mt_aget_float(mt_fid,mt_str_f2c('.'),mt_str_f2c('OmegaBaryon'),omega_baryon);
  call mt_aget_float(mt_fid,mt_str_f2c('.'),mt_str_f2c('OmegaCDM'),omega_cdm);
  call mt_aget_float(mt_fid,mt_str_f2c('.'),mt_str_f2c('OmegaLambda'),omega_lambda);
  call mt_aget_float(mt_fid,mt_str_f2c('.'),mt_str_f2c('Sigma8'),sigma8);
  call mt_aget_string(mt_fid,mt_str_f2c('.'),mt_str_f2c('Title'),title);

  ! Read snaptable from HDF file
  call mt_read_snaptable()

  ! Read merger tree from HDF file
  call mt_read_mergertree()

  ! Close file
  call mt_close()

end subroutine read_mock_data
