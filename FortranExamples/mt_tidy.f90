subroutine mt_tidy()

  use mt_data

! Deallocates open arrays.  Not strictly necessary

  deallocate(Snap)
  deallocate(mtHaloID)
  deallocate(mtSnapshot)
  deallocate(mtDensity)
  deallocate(mtMass)
  deallocate(mtFirstSubhaloIndex)
  deallocate(mtNeighbourIndex)
  deallocate(mtHostHaloIndex)
  deallocate(mtFirstProgenitorIndex)
  deallocate(mtNextSiblingIndex)
  deallocate(mtDescendantIndex)

end subroutine mt_tidy
